# intgrep

intgrep directly opens selected grep search result in a text editor

intgrep is short for interactive grep

![1](images/1.png)