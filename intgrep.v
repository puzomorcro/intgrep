import os
import strings
import strconv
import math

const (
  clear_format = '\x1b[0m'
)

fn line_to_test(line string) string {
  mut sb := strings.new_builder(line.len)
  mut skip := false
  for c in line {
    if !skip && int(c) == 27 {
      skip = true
    } else if skip && c == `m` {
      skip = false
      continue
    }

    if !skip {
      sb.write_b(c)
    }
  }
  return sb.str()
}

fn to_int(str string) (bool, int) {
  i := strconv.atoi(str)
  return i.str() == str, i
}

fn is_line_number_line(line string) bool {
  line_split := line.split(':')

  if line_split.len == 0 {
    return false
  }

  return strconv.atoi(line_split[0]).str() == line_split[0]
}

fn fetch(clean &array_string, index int) ?string {
  real_index := index
  if index < 0 || index - 1 >= clean.len {
    return none
  }

  line := clean[real_index]
  if line.trim_space().len == 0 {
    return none
  }

  maybe_line_number := line.split(':')

  mut line_number := 0
  mut filename := maybe_line_number[0]

  is_line_number, line_number_new := to_int(filename)
  if is_line_number {
    line_number = line_number_new
    for i in 0..real_index {
      filename = clean[real_index - (i + 1)].split(':')[0]
      if strconv.atoi(filename) == 0 {
        filename = os.realpath(filename)
        return '"$filename" $line_number'
      }
    }
    return none
  }

  filename = os.realpath(filename)
  return '"$filename" $line_number'
}

fn print_lines(lines, clean &array_string) {
  if lines.len == 0 {
    println('\tNo results')
  } else {
    println('')
    for i_line in 0..lines.len {
      str_line := if is_line_number_line(clean[i_line]) {
        ' '.repeat(1 + lines.len.str().len - i_line.str().len) + i_line.str()
      } else {
        ' '.repeat(1 + lines.len.str().len)
      }

      line := lines[i_line][0..int(math.min(160, lines[i_line].len))] + clear_format
      println('$str_line | $line')
    }
    println('')
  }
}

fn print_help() {
  println('\tq - quit')
  println('\tr - results')
  println('\th - this help')
  println('\t[number] - Peek')
  println('\t![number] - Neovim')
  println('\t%[number] - Visual Studio 17')
  println('\t[search_string] - ripgrep')
  println('\t:cd [path] - change directory')
}

fn exec_command(command, arguments string) ?os.Result {
  command_line := '$command $arguments'.replace('\\', '\\\\')
  r := os.exec(command_line) or {
    println('\t[$command_line] failed:]\n\t[$err]')
    return none
  }
  return r
}

fn main() {
  mut lines := []string
  mut clean := []string

  print_help()
  println('')
  println('')

  for {
    println(os.getwd())
    print('> ')
    mut input := os.get_line().trim_space()
    if input == 'q' {
      break
    }

    if input == 'r' {
      print_lines(lines, clean)
      continue
    }

    if input == 'h' {
      print_help()
      continue
    }

    mut tool := 'Peek'

    if input.starts_with(':cd ') {
      cd := os.realpath(input[4..])
      os.chdir(cd)
      continue
    }

    if input.starts_with('!') {
      input = input[1..]
      tool = 'Neovim'
    }

    if input.starts_with('%') {
      input = input[1..]
      tool = 'VSEdit.bat'
    }

    is_int, line_number := to_int(input)
    if is_int {
      arguments := fetch(clean, line_number) or {
        println('\tCan\'t parse')
        continue
      }

      r := exec_command(tool, '$arguments') or {
        continue
      }

      if r.output.len != 0 {
        println('')
        println(r.output)
        println('')
      }
      continue
    }

    if input.len != 0 {
      r := exec_command('rg --pretty', input) or {
        continue
      }

      if r.exit_code != 0 && r.exit_code != 1 {
        println('\t$r.output')
        println('\tExit code: $r.exit_code')
        continue
      }

      { // ??????????????????????
        lines_cpy := r.output.split_into_lines()
        lines = lines_cpy
        clean_cpy := lines.map(line_to_test(it))
        clean = clean_cpy
      }

      if lines.len > 200 {
        println('\tMore than 200 lines of output. Use `r` command to show them')
        continue
      }

      print_lines(lines, clean)
    }
  }
}
